package main

import (
	"flag"
	"fmt"
	"io"
	"io/fs"
	"log"
	"os"
	"path/filepath"
	"runtime"
	"strconv"
	"strings"
	"sync"
	"time"

	tailer "github.com/nxadm/tail"
	"github.com/radovskyb/watcher"
)

const (
	fileMode = iota
	directoryMode
	start
	stop
	stopAll
)

var (
	tails                sync.Map
	poll                 bool
	exclude              []string
	json                 bool
	minLevel             int
	pidToWatch           int
	truncateLogs         bool
	truncateLogsInterval int
	maxLogFileSize       int64
	watchCycle           time.Duration
	debugMode            bool
	readFromEnd          bool
	showVersion          bool
)

var (
	version   string
	buildtime string
)

// GetVersionString returns a standard version header
func GetVersionString() string {
	return fmt.Sprintf("gitlab-logger, version %v", version)
}

// GetVersion returns the semver compatible version number
func GetVersion() string {
	return version
}

// GetBuildTime returns the time at which the build took place
func GetBuildTime() string {
	return buildtime
}

type tailEvent struct {
	Op   int
	File string
}

func envValueString(key string, defaultVal string) string {
	if val, ok := os.LookupEnv(key); ok {
		return val
	}
	return defaultVal
}

func envValueBool(key string, defaultVal bool) bool {
	resVal := defaultVal
	if envVal, ok := os.LookupEnv(key); ok {
		if convVal, err := strconv.ParseBool(envVal); err == nil {
			resVal = convVal
		}
	}
	return resVal
}

func envValueInt(key string, defaultVal int) int {
	resVal := defaultVal
	if envVal, ok := os.LookupEnv(key); ok {
		if convVal, err := strconv.Atoi(envVal); err == nil {
			resVal = convVal
		}
	}
	return resVal
}

func init() {
	pollDefault := envValueBool("GITLAB_LOGGER_POLL", false)
	jsonDefault := envValueBool("GITLAB_LOGGER_JSON", false)
	truncateLogsDefault := envValueBool("GITLAB_LOGGER_TRUNCATE_LOGS", false)
	minLevelDefault := envValueInt("GITLAB_LOGGER_MINLEVEL", 0)
	wathcPidDefault := envValueInt("GITLAB_LOGGER_WATCH_PID", 0)
	truncateLogsIntervalDefault := envValueInt("GITLAB_LOGGER_TRUNCATE_INTERVAL", 300)
	maxLogFileSizeDefault := envValueInt("GITLAB_LOGGER_MAX_FILESIZE", 1000)
	readFromEndDefault := envValueBool("GITLAB_LOGGER_READ_FROM_END", false)
	// excludeDefault := envValueString("GITLAB_LOGGER_EXCLUDE", "sasl|config|lock|@|gzip|tgz|gz")
	exclude = []string{"sasl", "config", "lock", "@", "gzip", "tgz", "gz"}
	debugDefault := envValueBool("GITLAB_LOGGER_DEBUG", false)
	fs := flag.NewFlagSet("gitlab-logger", flag.ExitOnError)

	fs.BoolVar(&debugMode, "debug", debugDefault, "Be extra verbose. All debug information will be printed to stderr")
	fs.BoolVar(&showVersion, "version", false, "Print version and exit")
	fs.BoolVar(&poll, "poll", pollDefault, "Use polling instead of inotify")
	fs.BoolVar(&json, "json", jsonDefault, "Keep only JSON logs")
	fs.BoolVar(&readFromEnd, "read-from-end", readFromEndDefault, "Read from the end of files at startup, rather than all of the file")
	fs.BoolVar(&truncateLogs, "truncate-logs", truncateLogsDefault, "Truncate logs when they reach maximum allowed file size")
	fs.IntVar(&minLevel, "minlevel", minLevelDefault, "Minimum log level")
	fs.IntVar(&pidToWatch, "watch-pid", wathcPidDefault, "The PID of the process to watch")
	fs.IntVar(&truncateLogsInterval, "truncate-logs-interval", truncateLogsIntervalDefault, "Interval in seconds in which to truncate log files")
	fs.Int64Var(&maxLogFileSize, "max-log-file-size", int64(maxLogFileSizeDefault), "Maximum allowed log file size in bytes before file is truncated")
	fs.Func("exclude-keyword", "Exclude any file with full path containing keyword (could be added multiple times)", func(s string) error {
		exclude = append(exclude, s)
		return nil
	})
	// for backward compatibility we'll keep both "exclude" and "exclude-list"
	for _, option := range []string{"exclude-list", "exclude"} {
		var deprecation string
		if option == "exclude" {
			deprecation = "(DEPRECATED)"
		}
		fs.Func(option, deprecation+"Exclude list (use '|' as a separator): excludes any file with full path containing any of keywords provided", func(s string) error {
			exclude = strings.Split(s, "|")
			return nil
		})
	}
	// Below code prevents Usage screen from being displayed:
	// fs.SetOutput(ioutil.Discard)
	fs.Usage = func() { // [4]
		fmt.Fprintf(flag.CommandLine.Output(), "Usage of %s\n", os.Args[0])
		fs.PrintDefaults()
		fmt.Fprint(flag.CommandLine.Output(), `
Values for flags can be supplied by environment variables (see README )

CLI invocation flags overrides environment variable values`)
	}

	// parse and check for errors
	if err := fs.Parse(os.Args[1:]); err != nil {
		os.Exit(1)
	}

	if showVersion {
		fmt.Println(GetVersionString())
		os.Exit(0)
	}

	if debugMode {
		logger := log.New(os.Stderr, "", 0)
		logger.SetPrefix("DEBUG: ")
		logger.Print("Version: ", GetVersionString())
		logger.Print("Exclude: ", exclude)
		logger.Print("Poll: ", poll)
		logger.Print("Json: ", json)
		logger.Print("minLevel: ", minLevel)
		logger.Print("pidToWatch: ", pidToWatch)
		logger.Print("readFromEnd: ", readFromEnd)
		logger.Print("truncateLogs: ", truncateLogs)
		logger.Print("truncateLogsInterval: ", truncateLogsInterval)
		logger.Print("maxLogFileSize: ", maxLogFileSize)
	}
	watchCycle = time.Millisecond * 100
	log.SetFlags(0)
}

func main() {
	c := make(chan *tailEvent)
	ff := filesFromArgs()
	if pidToWatch > 0 {
		go watchPid(pidToWatch, c)
	}
	go tail(c)
	if v, ok := ff[directoryMode]; ok {
		go watch(v, c)
	}
	for _, f := range ff[fileMode] {
		c <- &tailEvent{Op: start, File: f}
	}

	if truncateLogs {
		go periodicTruncate(ff)
	}

	runtime.Goexit()
}

// truncate periodically sends truncate events to keep log files small.
func periodicTruncate(ff map[int][]string) {
	interval := time.Duration(truncateLogsInterval) * time.Second

	for range time.Tick(interval) {

		// Truncate specified files.
		for _, f := range ff[fileMode] {
			err := truncateFile(f, maxLogFileSize)
			if err != nil {
				log.Printf("unable to truncate file %s: %v\n", f, err)
			}
		}

		// Truncate specified directories.
		if dirs, ok := ff[directoryMode]; ok {
			for _, dir := range dirs {
				for _, f := range filesInDir(dir) {
					err := truncateFile(f, maxLogFileSize)
					if err != nil {
						log.Printf("unable to truncate file %s in directory: %v\n", f, err)
					}
				}
			}
		}
	}
}

func truncateFile(file string, maxBytes int64) error {
	f, err := os.Stat(file)
	if err != nil {
		return err
	}

	if f.Size() <= maxBytes {
		return nil
	}

	if err := os.Truncate(file, 0); err != nil {
		return err
	}

	return nil
}

func filesInDir(dir string) []string {
	result := []string{}

	walkDirFunc := func(path string, d fs.DirEntry, err error) error {
		if err != nil {
			return err
		}

		if d.IsDir() {
			return nil
		}

		result = append(result, path)

		return nil
	}

	if err := filepath.WalkDir(dir, walkDirFunc); err != nil {
		log.Printf("unable to walk directory %s: %v\n", dir, err)
	}

	return result
}

func watchPid(pid int, c chan *tailEvent) {
	pidDir := fmt.Sprintf("/proc/%d", pid)
	if _, err := os.Stat(pidDir); os.IsNotExist(err) {
		c <- &tailEvent{Op: stopAll}
		return
	}
	w := watcher.New()
	w.Add(pidDir)
	w.FilterOps(watcher.Remove)
	go func() {
		for {
			select {
			case err := <-w.Error:
				if err == watcher.ErrWatchedFileDeleted {
					c <- &tailEvent{Op: stopAll}
				} else {
					panic(err)
				}
			case <-w.Closed:
				return
			}
		}
	}()
	if err := w.Start(watchCycle); err != nil {
		panic(err)
	}
}

func watchFilter(info os.FileInfo, fullPath string) error {
	if info.IsDir() {
		return watcher.ErrSkip
	}
	for _, w := range exclude {
		if strings.Contains(fullPath, w) {
			return watcher.ErrSkip
		}
	}
	return nil
}

func watch(dirs []string, c chan *tailEvent) {
	w := watcher.New()
	w.AddFilterHook(watchFilter)
	w.FilterOps(watcher.Create, watcher.Write, watcher.Remove)
	go func() {
		for {
			select {
			case event := <-w.Event:
				switch event.Op {
				case watcher.Create, watcher.Write:
					_, ok := tails.Load(event.Path)
					if !ok {
						c <- &tailEvent{Op: start, File: event.Path}
					}
				case watcher.Remove:
					c <- &tailEvent{Op: stop, File: event.Path}
				default:
					panic(nil)
				}
			case err := <-w.Error:
				panic(err)
			case <-w.Closed:
				return
			}
		}
	}()
	for _, d := range dirs {
		if err := w.AddRecursive(d); err != nil {
			panic(err)
		}
	}
	if err := w.Start(watchCycle); err != nil {
		panic(err)
	}
}

func tail(c chan *tailEvent) {
	for evt := range c {
		switch evt.Op {
		case start:
			go func() {
				whence := io.SeekStart
				if readFromEnd {
					whence = io.SeekEnd
				}
				t, err := tailer.TailFile(evt.File, tailer.Config{Poll: poll, Follow: true, ReOpen: true, Location: &tailer.SeekInfo{Whence: whence}, Logger: tailer.DiscardingLogger})
				if err != nil {
					panic(err)
				}
				tails.Store(evt.File, t)
				for line := range t.Lines {
					logTo(t.Filename, line.Text)
				}
			}()
		case stop:
			v, ok := tails.Load(evt.File)
			if ok {
				v.(*tailer.Tail).Stop()
				tails.Delete(evt.File)
			}
		case stopAll:
			panic(fmt.Errorf("process %d has terminated, doing the same", pidToWatch))
		default:
			panic(nil)
		}
	}
}

func filesFromArgs() map[int][]string {
	var filePerMode map[int][]string = make(map[int][]string)
	for _, f := range os.Args[1:] {
		if strings.HasPrefix(f, "/") {
			mode := fileMode
			if isDir(f) {
				mode = directoryMode
			}
			filePerMode[mode] = append(filePerMode[mode], f)
		}
	}
	return filePerMode
}

func isDir(path string) bool {
	s, err := os.Stat(path)
	if err != nil {
		return false
	}
	return s.IsDir()
}

func logTo(file, message string) {
	if len(message) == 0 {
		return
	}
	j := isJSON(message)
	if json && !j {
		return
	}
	level, label := detectLevel(message)
	if level < minLevel {
		return
	}
	var line string
	if !j {
		line = fmt.Sprintf("\"time\":\"%s\",\"message\":%s", time.Now().Format(time.RFC3339), strconv.Quote(message))
	} else {
		line = message[1 : len(message)-1]
	}
	component, subcomponent := components(file)

	log.Printf("{\"component\": \"%s\",\"subcomponent\":\"%s\",\"level\":\"%s\",%s}\n", component, subcomponent, label, line)
}

func detectLevel(message string) (int, string) {
	level := 100
	label := "unknown"
	message = strings.ToLower(message)
	debug := []string{"debug"}
	info := []string{"info", "log", "get", "post", "processing", "starting", "started", "completed", "success", "saving", "saved", "creating", "created"}
	notice := []string{"notice"}
	warn := []string{"warn"}
	err := []string{"error", "failed"}
	fatal := []string{"fatal", "emerg"}
	for _, w := range debug {
		if strings.Contains(message, w) {
			level = 1
			label = "debug"
			break
		}
	}
	for _, w := range info {
		if strings.Contains(message, w) {
			level = 2
			label = "info"
			break
		}
	}
	for _, w := range notice {
		if strings.Contains(message, w) {
			level = 3
			label = "notice"
			break
		}
	}
	for _, w := range warn {
		if strings.Contains(message, w) {
			level = 4
			label = "warning"
			break
		}
	}
	for _, w := range err {
		if strings.Contains(message, w) {
			level = 5
			label = "error"
			break
		}
	}
	for _, w := range fatal {
		if strings.Contains(message, w) {
			level = 6
			label = "fatal"
			break
		}
	}
	return level, label
}

func components(file string) (string, string) {
	parts := strings.Split(file, "/")
	component := parts[len(parts)-2:][0]
	subcomponent := parts[len(parts)-1:][0]
	subcomponent = strings.TrimSuffix(subcomponent, filepath.Ext(subcomponent))
	return component, subcomponent
}

func isJSON(s string) bool {
	//cheap detection; must be good / fastest for such logs parsing
	return s[:1] == "{" && s[len(s)-1:] == "}"
}
