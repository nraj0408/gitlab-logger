GIT               := $(shell command -v git)
BUILD_TIME        := $(shell date +"%Y%m%d.%H%M%S")
LOGGER_VERSION    := $(shell ${GIT} describe 2>/dev/null | sed 's/^v//' || echo unknown)
GO_LDFLAGS        := -X main.version=${LOGGER_VERSION} -X main.buildtime=${BUILD_TIME}
INSTALL           := install
PREFIX            ?= /usr/local
prefix            ?= ${PREFIX}
exec_prefix       ?= ${prefix}
bindir            ?= ${exec_prefix}/bin
INSTALL_DEST_DIR  ?= ${DESTDIR}${bindir}

.PHONY: all
all: gitlab-logger

gitlab-logger: main.go
	go build -o "$@" -ldflags '${GO_LDFLAGS}'

.PHONY: install
install: gitlab-logger
	mkdir -p ${INSTALL_DEST_DIR}
	install gitlab-logger "${INSTALL_DEST_DIR}"
