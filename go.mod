module github.com/jbguerraz/gitlab-logger

go 1.13

require (
	github.com/nxadm/tail v1.4.4
	github.com/radovskyb/watcher v1.0.7
	golang.org/x/sys v0.3.0 // indirect
)
